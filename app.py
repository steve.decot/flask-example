import os
from flask import Flask
app = Flask(__name__)

@app.route("/")
def main():
    return "Bonjour !"

@app.route('/hello toi')
def hello():
    return 'Hey ! Je ne suis pas une app facile qui build le premier soir ! '

if __name__ == "__main__":
    app.run()
